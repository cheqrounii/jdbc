package persistance;
import java.sql.Connection;
import java.sql.SQLException;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;
import java.util.Scanner;

import com.mysql.cj.jdbc.result.ResultSetMetaData;



public class TestJDBC {
	
	
	          // Nom du driver JDBC et URL de la BD
	
	static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost:3306/Incident";
	// initialisation des variables de la BD
	static final String USER = "root";
	static final String PASS = "root";
	static Connection conn = null;
	
	           /* initConnection et closeConnection*/
	
	private static Connection initConnection(Connection conn) throws
	SQLException {
	try {
	// Enregistrer le JDBC Driver
	Class.forName(TestJDBC.JDBC_DRIVER);
	// Ouvrir la connection
	conn = DriverManager.getConnection(TestJDBC.DB_URL,
	TestJDBC.USER, TestJDBC.PASS);
	if(conn !=null) {
		System.out.println("connect� � la base de donn�es");
	}
	} catch (ClassNotFoundException e) {
	throw new SQLException("Impossible de trouver le driver JDBC: " + e.getMessage());
	}
	return conn;
	}



	private static void closeConnection(Connection conn) {
	try {
	if (conn != null) {
	conn.close();
	System.out.println("\n Fermeture de la connexion!");
	}
	} catch (SQLException e) {
	}
	}

	
	
	
	                       //main
	
	
	
	public static void main(String[] args) throws SQLException {
		

		String requete;
			
		try {
		TestJDBC.conn = TestJDBC.initConnection(TestJDBC.conn);

		/* Appel des m�thodes */

		/*
		 * requete = "INSERT INTO `Incident`.`Vehicule` (`code_interne`, `immatriculation`,\n" + 
				"`dateMiseEnCirculation`) VALUES ('848', 'DD-RB-JH', '2010-10-07 00:00:00');";
		try {
		   Statement stmt = conn.createStatement();
		   int nbMaj = stmt.executeUpdate(requete);
		   System.out.println("nombre    :"+ nbMaj +"\n");
		} catch (SQLException e) {
		   e.printStackTrace();
		}
		
*/
		//String date = "2019-09-01";
		//java.sql.Date SqlDate = java.sql.Date.valueOf(date);
		//TestJDBC.insererVehicule("1000", "12A45", SqlDate);
		
		//ModifierVehicule();

		//TestJDBC.listerVehicules();
		//System.out.println("\nla question 7 :"); rechercherVehicule();

	SupprimerVehicule();
		
		
		
		// partie 2 : 
		
		
		
		//Question 1
		
	/*System.out.println("Partie III : JDBC � Instructions param�tr�es");
		String date = "2018-09-01";
		java.sql.Date SqlDate = java.sql.Date.valueOf(date);
		TestJDBC.insererVehiculePS("8777", "15JH", SqlDate);
		*/
	
		//Question 2
		// modifierVehiculePSparId(1,"100");
		
		//Question 3 
	//	supprimerVehiculePSparId(92);
		
		//Question 5
		List<Vehicule> vehicules = TestJDBC. listerObjVehicule ();
		
		
		
		} catch (SQLException se) {
			
			
			
			
			
		// Erreurs du JDBC
			
		se.printStackTrace();
		} finally {
		TestJDBC.closeConnection(TestJDBC.conn);
		}
		
	}


	
	                         //****** les m�thodes *********
	
	
	
	private static void listerVehicules() {
		Statement stmt = null;
		// Exc�cution du requ�te
		System.out.println("Statement listerVehicule..."+"\n");
		try {
		stmt = TestJDBC.conn.createStatement();
		String sql;
		sql = "select id, code_interne, immatriculation,dateMiseEnCirculation from Vehicule;";
		ResultSet rs = stmt.executeQuery(sql);
		/*
		 * ResultSetMetaData rsmd=(ResultSetMetaData) rs.getMetaData();
		
		for(int i=1;i<=rsmd.getColumnCount();i++) {
			System.out.print(rsmd.getColumnName(i)+"\t");
		}			System.out.println("\n"); */
		// Extraction des donn�es du resultset
		while (rs.next()) {
		// par nom de colonne
		int id = rs.getInt("id");
		String codeInterne = rs.getString("code_interne");
		String immatriculation=rs.getString("immatriculation");
		String dateMiseEnCirculation =
				rs.getString("dateMiseEnCirculation");
				// Ecrire les valeurs
				System.out.print("ID: " + id+"\t");
				System.out.print(", Code interne: " + codeInterne+"\t");
				System.out.print(", Immatriculation : " +immatriculation+"\t");
				System.out.println(", Date de mise en circulation: " +dateMiseEnCirculation+"\t");
				} // Fermer ce qui est ouvert
				rs.close();
				stmt.close();
				} catch (SQLException se) {
				// erreurs du JDBC
				se.printStackTrace();
				} finally {
				// finally forcer la fermeture du statement
				try {
				if (stmt != null) {
				stmt.close();
				}
				} catch (SQLException se2) {
				}
				} // try
				}
	
	
	private static void insererVehicule(String ccc, String iii, Date SqlDate) {
		
		String requete;
		
		
		requete = "INSERT INTO `Incident`.`Vehicule` (`code_interne`, `immatriculation`,`dateMiseEnCirculation`) VALUES ("+ccc+",'"+iii+"','"+SqlDate+"')";
		try {
		   Statement stmt = conn.createStatement();
			
		   int nbMaj = stmt.executeUpdate(requete);
		} catch (SQLException e) {
		   e.printStackTrace();
		}
		
	}
	private static void rechercherVehicule() {
		
		Statement stmt = null;


		System.out.println("Statement listerVehicule...");
		Scanner sc = new Scanner(System.in);
		System.out.println("Veuillez saisir le code interne :");
		String code_interne = sc.nextLine();
		try {
		stmt = TestJDBC.conn.createStatement();
		String sql;
		sql = "select id, code_interne, immatriculation,dateMiseEnCirculation from Vehicule where code_interne = "+ code_interne + ";";
				System.out.println(sql);
				ResultSet rs = stmt.executeQuery(sql);
		
				while (rs.next()) {
					// par nom de colonne
					int id = rs.getInt("id");
					String codeInterne = rs.getString("code_interne");
					String immatriculation=rs.getString("immatriculation");
					String dateMiseEnCirculation =
							rs.getString("dateMiseEnCirculation");
							// Ecrire les valeurs
							System.out.print("ID: " + id+"\t");
							System.out.print(", Code interne: " + codeInterne+"\t");
							System.out.print(", Immatriculation : " +immatriculation+"\t");
							System.out.println(", Date de mise en circulation: " +dateMiseEnCirculation+"\t");
							} // Fermer ce qui est ouvert
							rs.close();
							stmt.close();
							} catch (SQLException se) {
							// erreurs du JDBC
							se.printStackTrace();
							} finally {
							// finally forcer la fermeture du statement
							try {
							if (stmt != null) {
							stmt.close();
							}
							} catch (SQLException se2) {
							}
							} 
		
						} 
	private static void ModifierVehicule()  {
String requete;

		System.out.println("Statement listerVehicule...");
		Scanner sc = new Scanner(System.in);
		System.out.println("Veuillez saisir le code interne :");
		String code_interne = sc.nextLine();
		
		System.out.println("Veuillez saisir nouveau immatriculation :");
		String newimmatriculation = sc.nextLine();
		System.out.println("Veuillez saisir nouveau date :");
		//String dateMiseEnCirculation = sc.nextLine();
		//System.out.println(dateMiseEnCirculation);*/
try {      Statement stmt = TestJDBC.conn.createStatement();

String requete1 =  "Update Vehicule Set immatriculation='"+newimmatriculation+"'  Where code_interne ='"+code_interne+"'";
Boolean ret = stmt.execute(requete1);

	ResultSet rs = stmt.executeQuery(requete1);

}catch (SQLException se2) {
}
		
	}
	
	//Question 11
	private static void SupprimerVehicule()  {
String requete;

		System.out.println("supprision d'un vehicule.");
		Scanner sc = new Scanner(System.in);
		System.out.println("Veuillez saisir le code interne de v�hicule que voudrais supprimer  :");
		String code_interne = sc.nextLine();
		
try {      Statement stmt = TestJDBC.conn.createStatement();

String requete1 =  "delete from Vehicule   Where code_interne ='"+code_interne+"'";

Boolean ret = stmt.execute(requete1);



}catch (SQLException se2) {
}
		
	}

		
	
	// partie 2 question 1 
	private static void insererVehiculePS(String _codeInterne,String _immatriculation, Date _dateMiseEnCirculation) throws SQLException {
		
		
		PreparedStatement stmt = null;
		String sql;
		
		try{
			sql = "insert into vehicule (code_interne,immatriculation, datemiseencirculation) values (?, ?, ?)";

		stmt = TestJDBC.conn .prepareStatement(sql);
		stmt.setString(1, _codeInterne);
		stmt.setString(2, _immatriculation);
		stmt.setDate(3, (java.sql.Date) _dateMiseEnCirculation);
		stmt.executeUpdate();
		stmt.close();	}catch (SQLException se2) {}}
	
	
	//partie 2 question 2 
	private static void modifierVehiculePSparId (long _id, String _codeInterne) throws SQLException {
		PreparedStatement stmt = null;
		String sql;
	
		try{
			sql="Update Vehicule Set code_interne=?  Where id =?";
		
		stmt = TestJDBC.conn .prepareStatement(sql);
		stmt.setString(1, _codeInterne);
		stmt.setLong(2, _id);
		stmt.executeUpdate();
		stmt.close();
	}catch (SQLException se2) {}
		}
	
     // la partie 2 Question 3 
	 public static void supprimerVehiculePSparId(long _id) {
		 PreparedStatement stmt = null;
			String sql;
		
			try{
				sql="delete from Vehicule  Where id =?";
			
			stmt = TestJDBC.conn .prepareStatement(sql);
			stmt.setLong(1, _id);
			System.out.println("supprission de "+_id);
			stmt.executeUpdate();
			
			stmt.close();
		}catch (SQLException se2) {}}



	 private static  List<Vehicule> listerObjVehicule() throws SQLException{
		 List<Vehicule> vehicules = new ArrayList<Vehicule>();
			PreparedStatement stmt = null;
			String sql="select * from Vehicule ";

			stmt = TestJDBC.conn .prepareStatement(sql);
			ResultSet rs=stmt.executeQuery();
			while(rs.next()) {
				Vehicule v=new Vehicule();
				v.setId(rs.getInt("id"));
				v.setCode_interne(rs.getString("code_interne"));
				v.setImmatriculation(rs.getString("immatriculation"));
				v.setDateMiseEnCirculation(rs.getDate("dateMiseEnCirculation"));
				vehicules.add(v);
				
				
			}
			for(int i=0;i<vehicules.size();i++) {
				
				System.out.print(vehicules.get(i).getId()+"\t");

				System.out.print(vehicules.get(i).getCode_interne()+"\t");
				System.out.print(vehicules.get(i).getImmatriculation()+"\t");
				
				System.out.println(vehicules.get(i).getDateMiseEnCirculation()+"\t");


			}
			return vehicules;
			
		 
	 }
	 
		 
		 
	 



}
	
	

				
	

	


