package persistance.dao;

import java.text.SimpleDateFormat;
import java.util.List;

public class TestVehiculeDAO {
	public static void main(String[] args) {
		TestVehiculeDAO.executer(VehiculeDAOFactory.JDBC_DAO);
	}

	public static void executer(String typeDao) {
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			// Initialiser une connexion
			VehiculeDAO vehiculeDao = VehiculeDAOFactory.getVehiculeDao(typeDao);
			// Rechercher tous les v�hicules

			List<Vehicule> listeTousLesVehicules = vehiculeDao.listerVehicules();

			// Afficher les v�hicules recherch�s
			for (Vehicule vehicule : listeTousLesVehicules) {
				System.out.println("#- id:" + vehicule.getId() + ",codeInterne:" + vehicule.getCode_interne()
						+ ", immat:" + vehicule.getImmatriculation() + ", dateMEC:"
						+ simpleDateFormat.format(vehicule.getDateMiseEnCirculation()));
			}

			// Question 8

			// inserertion
			// vehiculeDao.insererVehicule("44","jjh","2009-09-09");

			// modification

			// vehiculeDao.modifierVehiculeParId(3, "100");
			// supprission
			vehiculeDao.supprimerVehiculeParId(7);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}