package persistance.dao;

import java.util.Date;

public class Vehicule {
Long id;
String code_interne;
String immatriculation;

Date dateMiseEnCirculation;

public Vehicule(Long id, String code_interne, String immatriculation, Date dateMiseEnCirculation) {
	super();
	this.id = id;
	this.code_interne = code_interne;
	this.immatriculation = immatriculation;
	this.dateMiseEnCirculation = dateMiseEnCirculation;
}

public Vehicule() {
	super();
	// TODO Auto-generated constructor stub
}

public Long getId() {
	return id;
}

public void setId(Long id) {
	this.id = id;
}

public String getCode_interne() {
	return code_interne;
}

public void setCode_interne(String code_interne) {
	this.code_interne = code_interne;
}

public String getImmatriculation() {
	return immatriculation;
}

public void setImmatriculation(String immatriculation) {
	this.immatriculation = immatriculation;
}

public Date getDateMiseEnCirculation() {
	return dateMiseEnCirculation;
}

public void setDateMiseEnCirculation(Date dateMiseEnCirculation) {
	this.dateMiseEnCirculation = dateMiseEnCirculation;
}

}
