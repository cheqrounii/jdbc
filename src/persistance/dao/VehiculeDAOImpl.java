package persistance.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class VehiculeDAOImpl implements VehiculeDAO {
	private Connection connection;

	public VehiculeDAOImpl(Connection _connection) {
		this.connection = _connection;
	}

	@Override
	public List<Vehicule> listerVehicules() {
		Statement statement = null;
		try {
			String rechercherTousLesVehiculeQuery = "select v.* from vehicule v";
			statement = this.connection.createStatement();
			ResultSet resultSet = statement.executeQuery(rechercherTousLesVehiculeQuery);
			// Construire les objets Vehicule et les regrouper dans une liste
			List<Vehicule> listeTousLesVehicules = new ArrayList<>();
			while (resultSet.next()) {
				Vehicule vehicule = new Vehicule();
				vehicule.setId(resultSet.getLong("id"));
				vehicule.setCode_interne(resultSet.getString("code_interne"));
				vehicule.setImmatriculation(resultSet.getString("immatriculation"));
				vehicule.setDateMiseEnCirculation(resultSet.getDate("dateMiseEnCirculation"));
				listeTousLesVehicules.add(vehicule);
			}
			// Renvoyer en retour la liste des objets Vehicule construite
			return listeTousLesVehicules;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if ((statement != null) && !statement.isClosed()) {
					statement.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void finalize() throws Throwable {
		if ((this.connection != null) && !this.connection.isClosed()) {
			try {
				this.connection.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		super.finalize();
	}

	@Override
	public void insererVehicule(String _codeInterne, String _immatriculation, String _dateMiseEnCirculation) {
		String requete;

		requete = "INSERT INTO `Incident`.`Vehicule` (`code_interne`, `immatriculation`,`dateMiseEnCirculation`) VALUES ("
				+ _codeInterne + ",'" + _immatriculation + "','" + _dateMiseEnCirculation + "')";
		try {
			Statement stmt = connection.createStatement();

			int nbMaj = stmt.executeUpdate(requete);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void modifierVehiculeParId(long _id, String _codeInterne) throws SQLException {
		PreparedStatement stmt = null;

		try {
			String sql = "Update Vehicule Set code_interne=?  Where id =?";

			stmt = connection.prepareStatement(sql);
			stmt.setString(1, _codeInterne);
			stmt.setLong(2, _id);
			stmt.executeUpdate();
			stmt.close();

		} catch (SQLException se2) {
		}
	}
	/*
	 * methode une;
	 * 
	 * @Override public void supprimerVehiculeParId(long _id) {
	 * 
	 * String requete;
	 * 
	 * System.out.println("supprision d'un vehicule." + _id);
	 * 
	 * try { Statement stmt = connection.createStatement();
	 * 
	 * String requete1 = "delete from Vehicule   Where id ='" + _id + "'";
	 * 
	 * Boolean ret = stmt.execute(requete1);
	 * 
	 * } catch (SQLException se2) { } }
	 */

	// methode 2.
	@Override
	public void supprimerVehiculeParId(long _id) {
		String sql;
		try {
			sql = "delete from Vehicule   Where id =?";
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setLong(1, _id);
			ps.executeUpdate();
			ps.close();

		} catch (SQLException e) {

		}

	}
}