package persistance.dao;

import java.sql.SQLException;
import java.util.List;

public interface VehiculeDAO {
	List<Vehicule> listerVehicules();

	void insererVehicule(String _codeInterne, String _immatriculation, String _dateMiseEnCirculation);

	void modifierVehiculeParId(long _id, String _codeInterne) throws SQLException;

	void supprimerVehiculeParId(long _id);
}